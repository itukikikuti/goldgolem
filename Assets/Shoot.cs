﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField] GameObject block;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) || Input.GetKey(KeyCode.Return))
        {
            GameObject go = Instantiate(block, transform.position, Quaternion.identity);
            Rigidbody rb = go.GetComponent<Rigidbody>();
            rb.AddForce(transform.up * 2f, ForceMode.Impulse);
        }
    }
}
